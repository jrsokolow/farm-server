const db = require('../dbconnection')

var Client = {
  getClients: function (callback) {
    return db.query('select * from clients', callback)
  }
}

module.exports = Client
