const db = require('../dbconnection')

var Product = {
  getProducts: function (callback) {
    return db.query('select * from products', callback)
  }
}

module.exports = Product
