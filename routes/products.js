const express = require('express')
const router = express.Router()
const products = require('../models/product')

router.get('/', function (req, res, next) {
  products.getProducts(function (err, products) {
    if (err) {
      res.json(err)
    } else {
      res.json(products)
    }
  })
})

module.exports = router
