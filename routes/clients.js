const express = require('express')
const router = express.Router()
const clients = require('../models/client')

/* GET clients listing. */
router.get('/', function (req, res, next) {
  clients.getClients(function (err, rows) {
    if (err) {
      res.json(err)
    } else {
      res.json(rows)
    }
  })
})

module.exports = router
